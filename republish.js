const AWS = require('aws-sdk');

const { AWS_REGION } = process.env;
const IoT = new AWS.Iot();

exports.handler = async ({ topic, text }) => {
    const { endpointAddress } = await IoT.describeEndpoint({
        endpointType: 'iot:Data-ATS',
    }).promise();

    const IoTData = new AWS.IotData({
        region: AWS_REGION,
        endpoint: endpointAddress,
    });

    await IoTData.publish({
        topic,
        payload: JSON.stringify({
            text,
            lambda: true,
        }),
    }).promise();
};
