const AWS = require('aws-sdk');
const signature = require('aws-signature-v4');

const { AWS_REGION, ACCOUNT_ID, WEBSOCKET_ROLE, TOPIC } = process.env;
const IoT = new AWS.Iot();
const STS = new AWS.STS();

exports.handler = async (event) => {
    const { client } = event.pathParameters;
    const { endpointAddress } = await IoT.describeEndpoint({
        endpointType: 'iot:Data-ATS',
    }).promise();

    const {
        Credentials: {
            AccessKeyId,
            SecretAccessKey,
            SessionToken,
        },
    } = await STS.assumeRole({
        RoleArn: WEBSOCKET_ROLE,
        RoleSessionName: `WebSocketCredentials-${client}-${Date.now()}`,
        Policy: JSON.stringify({
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Action: 'iot:Connect',
                    Resource: `arn:aws:iot:${AWS_REGION}:${ACCOUNT_ID}:client/${client}`,
                },
                {
                    Effect: 'Allow',
                    Action: ['iot:Receive', 'iot:Publish'],
                    Resource: `arn:aws:iot:${AWS_REGION}:${ACCOUNT_ID}:topic/${TOPIC}/${client}`,
                },
                {
                    Effect: 'Allow',
                    Action: 'iot:Subscribe',
                    Resource: `arn:aws:iot:${AWS_REGION}:${ACCOUNT_ID}:topicfilter/${TOPIC}/${client}`,
                },
            ],
        }),
    }).promise();

    const url = signature.createPresignedURL(
        'GET',
        endpointAddress,
        '/mqtt',
        'iotdevicegateway',
        null,
        {
            key: AccessKeyId,
            secret: SecretAccessKey,
            sessionToken: SessionToken,
            protocol: 'wss',
            region: AWS_REGION,
        },
    );

    return {
        statusCode: 200,
        body: JSON.stringify({
            endpoint: url,
        }),
    };
};
