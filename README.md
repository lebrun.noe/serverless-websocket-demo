# Serverless Websocket Demo

This repository contain a small demo of a serverless websocket on AWS using AWS IoT.

The current behavior is just to re-send message published to the client to demonstrate the two way communication.

## Resources

- __API__: Simple API Gateway 
- __EndpointFunction__: API endpoint handler generating the WebSocket URI for a specific client
- __RepublishFunction__ Function triggered when a new message is published to an IoT topic

## Usage

The `/endpoint/{client}` endpoint will generate temporary AWS credentials for the client, allowing to subscribe only to specific topics. A signed WebSocket URI will then be generated using the AWS Signature v4.

The URI returned can be use with any MQTT client supporting the WebSocket protocol.

In this demo, the client is only allowed to subscribe to the following topic: `{Application name}/{Application stage}/{client}`.

## Deploy

The AWS Serverless Application Model is used to generate the infrastructure. Which means, the `sam` cli is required to create the package before deploying with CloudFormation.
